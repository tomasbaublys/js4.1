$(document).ready(function(){

    // Show panel on click

   $('#success').on('click', function(){

       $('.panel-success').removeClass('hidden');
       $('.panel-danger').addClass('hidden');
       $('.panel-warning').addClass('hidden');
   });

   $('#error').on('click', function(){

       $('.panel-danger').removeClass('hidden');
       $('.panel-success').addClass('hidden');
       $('.panel-warning').addClass('hidden');
   });

   $('#warning').on('click', function(){

       $('.panel-warning').removeClass('hidden');
       $('.panel-success').addClass('hidden');
       $('.panel-danger').addClass('hidden');
   });

   // Hide panel

   $('.panel .btn-danger').on('click', function(){

       $('.panel').addClass('hidden');
   });

   // Change panel body massage

   $('.panel-warning .btn-success').on('click', function(){

       $('.panel-warning .panel-body').text('This is a success!');
   });

   $('.panel-success .btn-success').on('click', function(){

       $('.panel-success .panel-body').text('This is a success!');
   });

   $('.panel-danger .btn-success').on('click', function(){

       $('.panel-danger .panel-body').text('This is a success!');
   });

});